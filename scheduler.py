import docker
import os
import time
import docker
import os
import time
import re
import GPUtil
import psutil

client = docker.from_env()
gpu_list = GPUtil.getGPUs()
gpu_list.remove(gpu_list[0])
gpu_list.remove(gpu_list[1])


def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split('(\d+)',text) ]

def get_experiments()->list:
    experiments = []
    for x in os.listdir('attacks'):
        if x.endswith(".yml"):
            experiments.append(x)
    return experiments

def start_conatiner(experiment: str, gpu: str, name: str):
    exp = 'palaestrai -c palaestrai.conf experiment-start  experiments_folder/%s'%experiment
    volumes = {"/home/OFFIS/nwenninghoff/considerations-on-reward-function-design":{'bind': '/workspace', 'mode': 'rw'}}
    env = ['PALAESTRAI_USER=nwenninghoff', 'PALAESTRAI_UID=10023', 'PALAESTRAI_GID=10143', 'PYTHONPATH=/workspace/src']
    c = client.containers.run('attack_paper',\
                              exp,\
                              device_requests=[\
                                               docker.types.DeviceRequest(
                                                    driver='nvidia',
                                                    device_ids=[gpu],
                                                    capabilities=[
                                                        ['gpu']
                                                    ]
                                                )
                                            ],\
                              volumes=volumes,\
                              detach=True,\
                              environment=env,\
                              remove=True,\
                              name=name,\
                              network='arl_palaestrai',\
                          )
    return c

new_experiments = get_experiments()
new_experiments.sort(key=natural_keys, reverse=True)
max_per_gpu = 5
max_gpu_id = 5
gpus = {gpu_list[0].uuid:0, gpu_list[1].uuid:0, gpu_list[2].uuid:0, gpu_list[3].uuid:0, gpu_list[4].uuid:0, gpu_list[5].uuid:0}


while new_experiments:
    gpus = {gpu_list[0].uuid:0, gpu_list[1].uuid:0, gpu_list[2].uuid:0, gpu_list[3].uuid:0, gpu_list[4].uuid:0, gpu_list[5].uuid:0}
    co = client.containers.list()
    for c in co:
        try:
            gpus[c.attrs['HostConfig']['DeviceRequests'][0]['DeviceIDs'][0]] +=1
        except TypeError:
            pass
    
    for i in range(len(gpus)):
        while gpus[gpu_list[i].uuid]<=max_per_gpu:
            stats = psutil.virtual_memory()
            free = getattr(stats, 'available')/getattr(stats, 'total')
            if free > 0.35:
                e = new_experiments.pop()
                c = start_conatiner(e, gpu_list[i].uuid, 'nwenninghoff-%s'%(e))
                print('Started new Experiment: %s on GPU: %d'%(e, i))
                gpus[gpu_list[i].uuid] +=1
                time.sleep(60)
            time.sleep(120)
