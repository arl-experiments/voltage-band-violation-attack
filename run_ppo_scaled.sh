#!/bin/bash

set -x

exec docker run \
    --network nwenninghoff_palaestrai \
    -v "$(dirname $(readlink -f "$0"))":/workspace \
    -e PALAESTRAI_USER=$(whoami) \
    -e PALAESTRAI_UID=$(id -u) \
    -e PALAESTRAI_GID=$(cut -d: -f3 < <(getent group "elab-projekt arl")) \
    -e PYTHONPATH=/workspace/src \
    --rm \
    --name $(whoami)-ppo_ttacks \
    --gpus device=0 \
    attack_paper:devel \
    palaestrai \
    -c palaestrai.conf \
    experiment-start \
    "$(dirname "$0")/attacks/attack1_ppo-2timeslots.yml"
    
docker run \
    --network nwenninghoff_palaestrai \
    -v "$(dirname $(readlink -f "$0"))":/workspace \
    -e PALAESTRAI_USER=$(whoami) \
    -e PALAESTRAI_UID=$(id -u) \
    -e PALAESTRAI_GID=$(cut -d: -f3 < <(getent group "elab-projekt arl")) \
    -e PYTHONPATH=/workspace/src \
    --rm \
    --name $(whoami)-ppo_ttacks \
    --gpus device=0 \
    attack_paper:devel \
    palaestrai \
    -c palaestrai.conf \
    experiment-start \
    "$(dirname "$0")/attacks/attack1_ppo-2timeslots_scaled.yml"