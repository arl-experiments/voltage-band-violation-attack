import logging
import math
import re
from typing import List

import numpy as np

from midas.tools.palaestrai.reward import Reward
from palaestrai.agent import SensorInformation, RewardInformation
from palaestrai.types import Box

LOG = logging.getLogger("palaestrai.agent.rew")


def gaus_norm(
        raw_value: float,
        mu: float = 1,
        sigma: float = 0.1,
        c: float = 0.5,
        a: float = -1,
):
    gaus_reward = a * math.exp(-((raw_value - mu) ** 2) / (2 * sigma ** 2)) - c
    return gaus_reward


class SingleLineLoadReward(Reward):
    @staticmethod
    def _line_load(value):
        if value <= 100:
            return value / 10
        if value > 100:
            return np.exp((value - 75) / 10)

    def __call__(self, state: List[SensorInformation], *args, **kwargs) -> List[RewardInformation]:
        reward = 0
        for sensor in state:
            if "line-" in sensor.sensor_id:
                r = self._line_load(sensor())
                if r > reward:
                    reward = r
        final_reward = RewardInformation(reward, Box(-100000, 100000, shape=(1,)), "SingleLineLoadReward")

        return [final_reward]


class CombinedLineLoadReward(Reward):

    @staticmethod
    def _line_load(value):
        if value <= 100:
            return value / 10
        if value > 100:
            return np.exp((value - 75) / 10)

    def __call__(self, state: List[SensorInformation], *args, **kwargs) -> List[RewardInformation]:
        reward = 0
        num = 0
        for sensor in state:
            if "line-" in sensor.sensor_id:
                reward += self._line_load(sensor())
                num += 1

        final_reward = RewardInformation(reward/num, Box(-9999999, 9999999, shape=(1,)), "CombinedLineLoadReward")

        return [final_reward]


class BusHealthReward(Reward):
    _RE = re.compile(r'\APowergrid-\d+.\d+-bus-\d+.vm_pu\Z')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 0

    def __call__(self, state: List[SensorInformation], *arg, **kwargs) -> List[RewardInformation]:
        sensors = [
            sensor
            for sensor in state
            if BusHealthReward._RE.match(sensor.sensor_id)
        ]
        #print("[%s] Sensors: %s" % (self.__class__, sensors))
        reward = 0
        for sensor in sensors:
            reward += sensor()
        reward = RewardInformation(
            reward/len(sensors),
            Box(-9999999, 9999999),
            "BusHealthReward"
        )
        #print("[%s][Step: %s] Reward: %s" % (self.__class__, self.step, reward))
        self.step += 1
        return [reward]


class MedianBusVoltageMagnitudeReward(Reward):
    SENSOR_ID_RE = re.compile(r'\APowergrid-\d+.\d+-bus-\d+.vm_pu\Z')
    VM_PU_MIN = 0.85
    VM_PU_MAX = 1.15

    def __call__(
            self,
            state: List[SensorInformation],
            *args,
            **kwargs
    ) -> List[RewardInformation]:
        sensors = [
            sensor
            for sensor in state
            if MedianBusVoltageMagnitudeReward.SENSOR_ID_RE.match(
                sensor.sensor_id
            )
        ]
        values = sorted([float(sensor()) for sensor in sensors])
        reward = values[len(values) // 2 - 1]
        #print("[%s] Reward: %s, Values: %s, Sensors: %s" %
        #      (
        #          "MedianBusVoltageMagnitudeReward",
        #          values,
        #          reward,
        #          sensors
        #      )
        #      )
        return [
            RewardInformation(
                reward,
                Box(0, 2),
                "MedianBusVoltageMagnitudeReward",
            )
        ]


class MaxBusVoltageMagnitudeReward(Reward):
    SENSOR_ID_RE = re.compile(r'\APowergrid-\d+.\d+-bus-\d+.vm_pu\Z')
    VM_PU_MIN = 0.85
    VM_PU_MAX = 1.15

    def __call__(
            self,
            state: List[SensorInformation],
            *args,
            **kwargs
    ) -> List[RewardInformation]:
        sensors = [
            sensor
            for sensor in state
            if MedianBusVoltageMagnitudeReward.SENSOR_ID_RE.match(
                sensor.sensor_id
            )
        ]
        values = sorted([float(sensor()) for sensor in sensors], key=lambda x: abs(1-x), reverse=True)
        reward = values[0]
        #print("[%s] Reward: %s, Values: %s" %
        #      (
        #          "MaxBusVoltageMagnitudeReward",
        #          reward,
        #          values,
        #      )
        #      )
        return [
            RewardInformation(
                reward,
                Box(0, 2),
                "MaxBusVoltageMagnitudeReward",
            )
        ]


class VoltageBandReward(Reward):
    def __call__(
        self,
        state: List[SensorInformation],
        *args,
        **kwargs
    ) -> List[RewardInformation]:
        rewards = [
            RewardInformation(
                float(sensor()),
                Box(0.0, 2.0),
                sensor.sensor_id,
            )
            for sensor in state
            if str(sensor.sensor_id).endswith("vm_pu")
        ]
        LOG.debug("Rewards: %s", rewards)
        return rewards
