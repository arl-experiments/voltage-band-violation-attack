import logging

from palaestrai.agent import Muscle
from palaestrai.types import Box
from .reactive_power_muscle import ReactivePowerMuscle

LOG = logging.getLogger(
    "palaestrai.agent.Muscle.AlternatingValueAttacker"
)


class AlternatingValueAttacker(Muscle):
    HOLDOFF_STEPS = ReactivePowerMuscle.STEP_SIZE * 2

    def __init__(
        self,
        broker_uri,
        brain_uri,
        uid,
        brain_id,
        holdoff_steps=HOLDOFF_STEPS
    ):
        super().__init__(broker_uri, brain_uri, uid, brain_id)
        self._holdoff_steps = holdoff_steps
        self._state_counter = 0
        self._current_slope = 1

    def setup(self):
        pass

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        self._state_counter += 1
        if self._state_counter >= self._holdoff_steps:
            self._current_slope = -1 * self._current_slope
            self._state_counter = 0

        for actuator in actuators_available:
            asp: Box = actuator.action_space
            high = asp.high[0]
            actuator([high * self._current_slope])

        LOG.debug(
            "%s: Slope: %s, Counter: %s, Actuators: %s",
            self,
            self._current_slope,
            self._state_counter,
            actuators_available
        )

        return (
            actuators_available,
            list(),
            list(),
            dict()
        )

    def update(self, update):
        pass

    def prepare_model(self):
        pass

    def __repr__(self):
        pass
