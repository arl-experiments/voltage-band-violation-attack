from typing import List

from palaestrai.agent import Muscle, SensorInformation, ActuatorInformation


class MaxValueTester(Muscle):

    def __init__(self, broker_uri, brain_uri, uid, brain_id, path):
        super().__init__(broker_uri, brain_uri, uid, brain_id, path)
        self.old_state = None

    def setup(self):
        pass

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        if actuators_available is not None:
            actuator_list = []
            for actuator in actuators_available:
                actuator.setpoint = 100
                actuator_list.append(actuator)
            return actuator_list, list(), list(), dict()
        else:
            return list(), list(), list(), dict()

    def update(self, update):
        pass

    def prepare_model(self):
        pass

    def __repr__(self):
        pass