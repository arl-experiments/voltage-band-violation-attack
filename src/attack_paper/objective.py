import logging
from functools import partial
from typing import List, Dict

import numpy as np

from palaestrai.agent import RewardInformation
from palaestrai.agent.memory import Memory
from palaestrai.agent.objective import Objective
from .gauss import normal_distribution_pdf

LOG = logging.getLogger("palaestrai.agent.objective")


class VoltageBandMaintenanceObjective(Objective):
    """Defender Objective: Try to maintain a good voltage."""

    def __init__(self, params):
        super().__init__(params)
        self._our_sensors = params.get("sensors", None)
        self._gauss = partial(
            normal_distribution_pdf,
            mu=params.get("mu", 1.0),
            sigma=params.get("sigma", 0.032),
            c=params.get("c", 0.0),
            a=params.get("a", 10.0),
        )
        self.step = 0

    def internal_reward(self, rewards: List[RewardInformation], **kwargs):
        if self._our_sensors is not None:
            reward_filter = lambda reward: any(
                sid in str(reward.reward_id)
                for sid in self._our_sensors
            )
        else:
            reward_filter = lambda reward: str(reward.reward_id).endswith(
                "vm_pu"
            )
        applicable_rewards = np.array([
            r.reward_value
            for r in rewards
            if reward_filter(r)
        ])
        reward = self._gauss(np.median(applicable_rewards))
        LOG.debug(
            "[%s][Step: %s] "
            "Objective: %s, "
            "applicable rewards: %s, "
            "all rewards: %s",
            self,
            self.step,
            reward,
            applicable_rewards,
            rewards,
        )
        self.step += 1
        return reward


class VoltageBandViolationObjective(Objective):
    def __init__(self, params: Dict):
        super().__init__(params)
        self._our_sensors = params.get("sensors", None)
        self._objective_scaling = params.get("objective_scaling", 1.0)
        self._gauss = partial(
            normal_distribution_pdf,
            mu=params.get("mu", 1.0),
            sigma=params.get("sigma", -0.05),
            c=params.get("c", -10.0),
            a=params.get("a", -12.0),
        )
        self.step = 0

    def internal_reward(self, rewards: List[RewardInformation], **kwargs):
        """Expect Voltage values and reward voltage band violations."""
        if self._our_sensors is not None:
            reward_filter = lambda reward: any(
                sid in str(reward.reward_id)
                for sid in self._our_sensors
            )
        else:
            reward_filter = lambda reward: str(reward.reward_id).endswith(
                "vm_pu"
            )
        applicable_rewards = np.array([
            r.reward_value
            for r in rewards
            if reward_filter(r)
        ])
        reward = self._gauss(np.median(applicable_rewards))
        LOG.debug(
            "[%s][Step: %s] "
            "Objective: %s, "
            "applicable rewards: %s, "
            "all rewards: %s",
            self,
            self.step,
            reward,
            applicable_rewards,
            rewards,
        )
        self.step += 1
        return reward


class VoltageBandViolationPendulum(Objective):
    VM_PU_LOW = 0.85
    VM_PU_HIGH = 1.15
    SIGMA = -0.05
    C = -10.0
    A = -12.0

    def __init__(self, params: Dict):
        super().__init__(params)
        self._our_sensors = params.get("sensors")
        self._sigma = params.get("sigma", VoltageBandViolationPendulum.SIGMA)
        self._c = params.get("c", VoltageBandViolationPendulum.C)
        self._a = params.get("a", VoltageBandViolationPendulum.A)
        self.step = 0

    def objective_from_vm(self, vm_pu: float):
        objective = normal_distribution_pdf(
            x=vm_pu,
            mu=1.0,
            sigma=self._sigma,
            c=self._c,
            a=self._a
        )
        objective += normal_distribution_pdf(
            x=vm_pu,
            mu=0.83,
            sigma=0.01,
            c=0.0,
            a=self._a
        )
        objective += normal_distribution_pdf(
            x=vm_pu,
            mu=1.16,
            sigma=0.01,
            c=0.0,
            a=self._a
        )
        return objective

    def internal_reward(self, memory: Memory, **kwargs):
        """Expect Voltage values and reward voltage band violations."""
        if self._our_sensors:
            sensors = [
                agent_sensor
                for our_sensor in self._our_sensors
                for agent_sensor in memory.get_rewards().columns
                if our_sensor in agent_sensor
            ]
            rewards = memory.get_rewards()[-1:][sensors]
        else:
            rewards = memory.get_rewards()[-1:].filter(
                like=".vm_pu", axis=1
            )
        vm_pu = float(rewards.median(axis=1))
        objective = self.objective_from_vm(vm_pu)
        self.step += 1
        return objective

