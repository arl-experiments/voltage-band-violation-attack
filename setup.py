import os
from setuptools import find_packages, setup

with open("VERSION") as freader:
    VERSION = freader.readline().strip()

with open("README.rst") as freader:
    README = freader.read()

setup(
    name="Palaestrai-Reactive-Power-Attack",
    version=VERSION,
    description="Reactive Power Attack",
    long_description=README,
    author="Nils Wenninghoff <nils.wenninghoff@offis.de>",
    author_email="nils.wenninghoff@offis.de",
    python_requires=">=3.8.0",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    install_requires=[
        "torch>=1.8.1",
        "numpy>=1.18.5",
    ],
    license="LGPL",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: "
        "GNU Lesser General Public License v3 (LGPLv3)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ],
)