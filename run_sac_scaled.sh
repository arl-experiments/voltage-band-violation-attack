#!/bin/bash

set -x

exec docker run \
    --network nwenninghoff_palaestrai \
    -v "$(dirname $(readlink -f "$0"))":/workspace \
    -e PALAESTRAI_USER=$(whoami) \
    -e PALAESTRAI_UID=$(id -u) \
    -e PALAESTRAI_GID=$(cut -d: -f3 < <(getent group "elab-projekt arl")) \
    -e PYTHONPATH=/workspace/src \
    --rm \
    --name $(whoami)-sac_attacks \
    --gpus device=0 \
    attack_paper:devel \
    palaestrai \
    -c palaestrai.conf \
    experiment-start \
    "$(dirname "$0")/attacks/attack1_sac-2timeslots.yml"
   
docker run \
    --network nwenninghoff_palaestrai \
    -v "$(dirname $(readlink -f "$0"))":/workspace \
    -e PALAESTRAI_USER=$(whoami) \
    -e PALAESTRAI_UID=$(id -u) \
    -e PALAESTRAI_GID=$(cut -d: -f3 < <(getent group "elab-projekt arl")) \
    -e PYTHONPATH=/workspace/src \
    --rm \
    --name $(whoami)-sac_attacks \
    --gpus device=0 \
    attack_paper:devel \
    palaestrai \
    -c palaestrai.conf \
    experiment-start \
    "$(dirname "$0")/attacks/attack1_sac-2timeslots_scaled.yml"



