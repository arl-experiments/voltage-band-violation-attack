NAME			:= der-attacks
GIT_DATE		:= $(shell git show -s --format=%cs HEAD)
GIT_CUR_HASH 	:= $(shell git rev-parse --short HEAD)
EXPERIMENT		?= attacks/endgame.yml
GPUS			?= device=5


.PHONY: run
run: .deps/docker-image-$(GIT_CUR_HASH)
	docker run  -v "$$(pwd)":/workspace \
		-e PALAESTRAI_USER="$$(whoami)" \
		-e PALAESTRAI_UID=$$(id -u) \
		-e PALAESTRAI_GID=10143 \
		-e PYTHONPATH=/workspace/src  \
		--rm \
		--name "$$(whoami)-$(NAME)" \
		--gpus "$(GPUS)" \
		"der-attacks:$(GIT_DATE)-$(GIT_CUR_HASH)" \
		/opt/conda/bin/palaestrai \
		-c palaestrai.conf \
		experiment-start \
		"$(EXPERIMENT)"


.deps/docker-image-$(GIT_CUR_HASH): docker/Dockerfile .git/index requirements.txt .deps
	docker build \
		--no-cache \
		-t "$(NAME):$(GIT_DATE)-$(GIT_CUR_HASH)" \
		-f docker/Dockerfile \
		.
	touch .deps/docker-image-$(GIT_CUR_HASH)


.deps:
	mkdir .deps

